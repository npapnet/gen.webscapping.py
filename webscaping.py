#%% Web Scraping HTML Tables with Python
#
# steps
# - Inspect HTML
# - Import Libraries
# - Scrape Table Cells
# - Parse Table Header
# - Creating Pandas DataFrame

#%%
import requests
import lxml.html as lh
import pandas as pd
import numpy as np
#%%
url='http://www.worldometers.info/coronavirus/'
#Create a handle, page, to handle the contents of the website
page = requests.get(url)
#Store the contents of the website under doc
doc = lh.fromstring(page.content)
#Parse data that are stored between <tr>..</tr> of HTML
tr_elements = doc.xpath('//tr')

# %%
# Sanity Check the length of the first 12 rows
sc = np.array([len(T) for T in tr_elements[:]])
# if []
if (sc.mean()==sc).all() and sc.mean()>0:
    print ('all rows have the same (non zero) size')
sc_size = int(sc.mean())

# %% Parse Table Header
# tr_elements = doc.xpath('//tr')
#Create empty list
col=[]
i=0
#For each row, store each first element (header) and an empty list
for t in tr_elements[0]:
    i+=1
    name=t.text_content()
    name = name.replace("\n","")
    print('{}:{}'.format(i,name))
    col.append((name,[]))
col[12]

#Since out first row is the header, data is stored on the second row onwards
# for j in range(1,len(tr_elements)):
    #T is our j'th row
    # T=tr_elements[j]
for T in tr_elements[1:]:

    #If row is not of size 10, the //tr data is not from our table 
    if len(T)!=sc_size:
        break
    
    #i is the index of our column
    i=0
    
    #Iterate through each element of the row
    for t in T.iterchildren():
        data=t.text_content().replace("\n", " ")
        #Check if row is empty
        if i>0:
        #Convert any numerical value to integers
            try:
                data=int(data.replace(",","").strip())
            except:
                # Adding this check to allow the country and continent string to 
                if not col[i][0] in [col[1][0],col[15][0]]:
                    data=np.nan
                pass
        #Append the data to the empty list of the i'th column
        col[i][1].append(data)
        #Increment i for the next column
        i+=1

[len(C) for (title,C) in col]
Dict={title:column for (title,column) in col}
df=pd.DataFrame(Dict)
df.head()
# %%
df.describe()



# %%
df.to_csv('result.csv')

