# Web Scraping HTML Tables with Python

This is a repo for web scraping HTML Tables with python.

The original motive was based for obtaining data from  [worldmeters.info.](https://www.worldometers.info/coronavirus/).

The code originally was based on the online article with the same title from [Syed Sadat Nazrul](https://towardsdatascience.com/web-scraping-html-tables-with-python-c9baba21059) for python 2.

This has been modified for:

- Python 3
- "Wordlmeters.info/coronavirus" webpage. 



